import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ProductionModel } from '../model/production.model';
import { timeout } from 'rxjs/operators';


@Injectable()
export class ProductionService {

    constructor(private http: HttpClient) { }
    private url: string = 'http://192.168.200.171:8012/rest/WBROWSEOP';

    public post(production: ProductionModel, address: string): Observable<any> {
        const httpHeaders: HttpHeaders = new HttpHeaders();
        const header: HttpHeaders = this.createAuthorizationHeader(httpHeaders);
        return this.http.post(address? address: this.url, production, { headers: header, observe: 'response', responseType: 'json' })
        .pipe(timeout(15000));
    }

    private createAuthorizationHeader(headers: HttpHeaders) {
        const admin = {
            name: 'admin',
            password: '27782778'
        }
        let user = btoa(admin.name + ":" + admin.password);

        return headers.append('Authorization', 'Basic ' + user).append('Access-Control-Allow-Origin', '*');
    }
}