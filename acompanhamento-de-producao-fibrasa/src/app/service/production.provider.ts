import { Injectable } from '@angular/core';
import { PoModalComponent } from '@po-ui/ng-components';
import { ProductionModel } from '../model/production.model';

@Injectable()
export class ProductionProvider {
  constructor() {}

  productionStoraged: any = {};
  paramsStoraged: any = {};

  public modalCustom(poModal: PoModalComponent, title?: string) {
    poModal.title = title;
    poModal.open();
  }

  public setStorageItems(items: any): void {
    const itemsStoraged = JSON.stringify(items);
    window.localStorage.setItem('itemsStoraged', itemsStoraged);
  }

  public getStorageItems(): void {
    const currentitems = window.localStorage.getItem('itemsStoraged');
      return this.productionStoraged = JSON.parse(currentitems);
  }

  public setStorageParams(params: ProductionModel): void{
    const paramsStoraged = JSON.stringify(params);
    window.localStorage.setItem('paramsStoraged', paramsStoraged);
  }

  public getStorageParams(): any {
    const currentParams = window.localStorage.getItem('paramsStoraged');
    return this.paramsStoraged = JSON.parse(currentParams);
  }}
