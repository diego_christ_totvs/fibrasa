import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { ProductionRoutingModule } from './production-routing.module';
import { ProductionComponent } from './production.component';
import { PoModule } from '@po-ui/ng-components';
import { ProductionService } from './service/production.service';
import { ProductionProvider } from './service/production.provider';

@NgModule({
  declarations: [
    ProductionComponent
  ],
  imports: [
    BrowserModule,
    ProductionRoutingModule,
    PoModule,
    FormsModule,
  ],
  providers: [ProductionService, {provide: ProductionProvider, useClass: ProductionProvider}],
  bootstrap: [ProductionComponent]
})
export class ProductionModule { }
