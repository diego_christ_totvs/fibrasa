import { Component, ViewChild, OnInit } from '@angular/core';
import {
  PoBreadcrumb,
  PoModalComponent,
  PoModalAction,
} from '@po-ui/ng-components';
import { ProductionService } from './service/production.service';
import { ProductionModel } from './model/production.model';
import { ProductionProvider } from './service/production.provider';

@Component({
  selector: 'production-root',
  templateUrl: './production.component.html',
  preserveWhitespaces: true,
})
export class ProductionComponent implements OnInit {
  @ViewChild(PoModalComponent, { static: true }) poModal: PoModalComponent;

  public timerMin: number;
  private timerSec: number = 59;

  public address: string = '';

  public source: string = '';
  public destination: string = '';
  public difference: string = 'N';

  public switchChecked: boolean;
  public loading: boolean = false;

  public message: string = '';
  public titleModal: string = '';

  public disabledFilter: boolean = false;

  public params: ProductionModel;
  public paramsStoraged: any;
  public intervalId: any;

  public initServer: boolean;

  constructor(private prodService: ProductionService, private prodProvider: ProductionProvider) {
    this.paramsStoraged = this.prodProvider.getStorageParams();
  }

  public counter: { min: any, sec: any }

  public readonly filter: PoBreadcrumb = {
    items: [{ label: 'Filtro' }],
  };

  public readonly moviments: PoBreadcrumb = {
    items: [{ label: 'Movimentos' }],
  };

  public items: any = [];

  public readonly primayAction: PoModalAction = {
    action: () => {
      this.initServer = true;
      if (this.params) {
        this.params.ADDRESS = this.address;
        this.params.TIME = this.timerMin
      }
      this.prodProvider.setStorageParams(this.params);
      this.enableDisplay();
      this.startTimer(this.timerMin);
      this.poModal.close();

    },
    label: 'Ok',
  };

  public readonly secondaryAction: PoModalAction = {
    action: () => {
      this.poModal.close();
    },
    label: 'Cancelar',
  };

  async ngOnInit() {
    if (this.paramsStoraged != null || this.initServer) {
      try {
        this.items = await this.getProduction(this.paramsStoraged.LOCALORIGEM, this.paramsStoraged.LOCALDESTINO, this.paramsStoraged.TIME, this.paramsStoraged.ADDRESS);
      } catch (error) {
        this.items = await this.getProduction(this.params.LOCALORIGEM, this.params.LOCALDESTINO, this.params.TIME, this.params.ADDRESS);
      }
    } else {
      this.insertInfo(this.poModal, 'Dados de Entrada Opcionais');
    }
  }

  async startTimer(time?: number) {
    clearInterval(this.intervalId);
    this.timerMin <= 0 ? this.timerMin = 1 : this.timerMin;
    this.counter = { min: time ? time - 1 : 29, sec: this.timerSec }

    if (this.paramsStoraged || this.params) {
      this.intervalId = setInterval(async () => {
        if (this.counter.sec - 1 == -1) {
          this.counter.min -= 1;
          this.counter.sec = 59
        }
        else this.counter.sec -= 1;

        if (this.counter.sec < 10) {
          this.counter.sec = '0' + this.counter.sec;
        }
        if (this.counter.min === 0 && this.counter.sec === '00') {
          clearInterval(this.intervalId);
          this.paramsStoraged = this.prodProvider.getStorageItems();
          this.ngOnInit();
        }
      }, 1000)
    }
  }

  public readonly fields: Array<any> = [
    { property: 'ID', label: '#' },
    { property: 'COP', label: 'N° OP' },
    { property: 'CPRODUTO', label: 'Codigo' },
    { property: 'CDESCRICAO', label: 'Descrição' },
    { property: 'CQTDOP', label: 'Qtde.Total OP' },
    { property: 'CQTDORIG', label: 'Local ' + this.source },
    { property: 'CQTDDEST', label: 'Local ' + this.destination },
    { property: 'CDIF', label: 'Diferença' },
  ];

  public changeDifference(): void {
    this.switchChecked ? (this.difference = 'S') : (this.difference = 'N');
  }

  public disabledBtn() {
    typeof Number(this.timerMin) != "number" ? this.primayAction.disabled = true : false;
  }

  public enableDisplay() {
    document.getElementById("hidden").style.display = '';
    document.getElementById("paramsbtnfilter").style.display = 'none';
    document.getElementById("paramsbtnnofilter").style.display = '';
  }

  public disableDisplay() {
    document.getElementById("hidden").style.display = 'none';
    document.getElementById("paramsbtnfilter").style.display = '';
    document.getElementById("paramsbtnnofilter").style.display = 'none';

  }

  public getProduction(source: string, destination: string, time?: number, address?: string): Promise<any[]> {
    this.disableDisplay();
    this.initServer = true;
    this.counter = undefined;
    clearInterval(this.intervalId);
    this.items = [];
    this.loading = true;

    const errorServer: string = 'Servidor não está ativo. Verifique a conexão.';
    const errorServerTime: string = 'Servidor não respondeu no tempo limite. Verifique a conexão.';
    const errorNoData: string = 'Não foram encontrados dados com o filtro informado ou o endereço do serviço está incorreto.';
    const errorFields: string = 'Por favor, preencha os campos corretamente.';
    const titleFailure: string = 'Falha na obtenção dos dados: ';

    this.source = source;
    this.destination = destination;
    this.destination = destination;
    this.timerMin = this.timerMin ? this.timerMin : time;
    this.address = this.address ? this.address : address;

    this.params = {
      LOCALORIGEM: this.source,
      LOCALDESTINO: this.destination,
      DIFERENCA: this.difference,
      TIME: this.timerMin,
      ADDRESS: this.address
    };

    this.fields.find((element) => {
      if (element.property === 'CQTDORIG') {
        element.label = 'Local ' + source;
      }

      if (element.property === 'CQTDDEST') {
        element.label = 'Local ' + destination;
      }
    });

    this.prodProvider.setStorageParams(this.params);

    return new Promise((resolve) => {
      this.prodService.post(this.params, this.params.ADDRESS).subscribe(
        (items: any) => {
          if (items.body) {
            items.body.forEach(element => {
              element.ID = items.body.indexOf(element) + 1;
            });

            this.items = items.body;
            resolve(items.body);
            this.loading = false;

            this.startTimer(this.timerMin);
          }
        },
        (error) => {
          this.loading = false;
          if (error.status === 404) {
            this.resolveRequestError(this.poModal, titleFailure + error.status, errorNoData);
          } else if (error.name === 'TimeoutError') {
            this.resolveRequestError(this.poModal, titleFailure + (error.status ? error.status : error.message), errorServerTime);
          } else {
            this.resolveRequestError(this.poModal, titleFailure + error.status, errorServer);
          }
          this.items.length = 0
        }
      );
    });
  }

  private resolveRequestError(poModal: PoModalComponent, titleFailure: string, message: string): void {
    this.loading = false
    this.prodProvider.modalCustom(poModal, titleFailure);
    this.message = message;
  }

  public insertInfo(poModal: PoModalComponent, titleInfo: string, message?: string): void {
    this.initServer = false;
    this.loading = false
    this.prodProvider.modalCustom(poModal, titleInfo);
    this.message = message;
  }

  public showMore(): void {
    this.getProduction(this.source, this.destination).then((elements: any[]) => {
      elements.forEach((item) => {
        if (this.items.indexOf(item) < 0) {
          this.items.push(item);
        }
      });
    });
  }
}
