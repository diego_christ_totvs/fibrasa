export interface ProductionModel {
    LOCALORIGEM?: string;
    LOCALDESTINO?: string;
    DIFERENCA?: string;
    TIME?:number;
    ADDRESS?:string;
}